# Spring Boot - Spring Framework 5 #
## Spring Boot Web Application - Spring Boot Horario ##

This repository contains a Spring Boot Web Application in which the application allows to Sign In or not depending on a predefined timetable following the next
Udemy Course [Spring Framework 5: Creando webapp de cero a experto (2020)](https://www.udemy.com/course/spring-framework-5/).

### Tools ###

* Spring Tool Suite 4
* Spring Boot v2.3.5
* Spring Boot Dev Tools
* Thymeleaf
* MySQL Connector v8.0.21
* JDK v11.0.9
* Apache Maven
